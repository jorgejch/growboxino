#include <SimpleTimer.h>   // Timer library.
#include <DHT.h>           // H/T sensor library. 
#include <QueueList.h>     // List library for Arduino.
#include <LiquidCrystal.h> // Display library.

#define DHTPIN 6    // What pin is the DHT humidity/temperature sensor using?
#define HMDPIN 7    // What pin is the humidifier on?
#define CLPIN 8     // What pin is the cooler (Peltie) relay connected to?
#define HTPIN 9     // What pin is the Heating matt relay connected to?
// What DHT version I am using?
//#define DHTTYPE DHT11   // DHT 11 
#define DHTTYPE DHT22   // DHT 22  (AM2302) 
//#define DHTTYPE DHT21   // DHT 21 (AM2301)
// Defines the current temperature limits (uppper and lower).
#define TEMP_UPPER_LIMIT 28
#define TEMP_LOWER_LIMIT 20
// Defines the current humidity limit (upper and lower).
#define HUMID_UPPER_LIMIT 99
#define HUMID_LOWER_LIMIT 87

// Instantiates the DHT sensor object, with the DHT pin number, and type.
DHT dht(DHTPIN, DHTTYPE);
// Instantiantes the Display object with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);


// Instantiate timer.
SimpleTimer timer;

int mist_timer;
int print_timer;
long init_milli;
float h, t;

// Verifies if box needs cooling.
boolean coolBox(float current_temp){
  if (current_temp > TEMP_UPPER_LIMIT)
      return true;
  return false;
}

// Turns cooling on or off.
void setCooling(boolean power_on){
  if (power_on) {
    digitalWrite(CLPIN, HIGH);
    Serial.print("Cooling is ON.\n");
  }
  else {
    digitalWrite(CLPIN, LOW);
    Serial.print("Cooling is OFF.\n");
  }  
  return;
}

// Verifies if the box needs to be heated.
boolean heatBox(float current_temp) {
  if (current_temp < TEMP_LOWER_LIMIT)
      return true;
  return false;
}

// Turns heating on or off.
void setHeating(boolean power_on){
  if (power_on){
    digitalWrite(HTPIN, HIGH);
    Serial.print("Heating is ON.\n");
  }
  else {
    digitalWrite(HTPIN, LOW);
    Serial.print("Heating is OFF.\n");
  }
  return;
}

// Checks to see if cooling is on.
boolean isCooling(){
  if (digitalRead(CLPIN))
    return true;
  return false;
}

// Checks to see if heating is on.
boolean isHeating(){
  if (digitalRead(HTPIN))
    return true;
  return false;
}

// Verifies if the box needs to be humidified.
boolean mistBox(){
  if (h >= HUMID_LOWER_LIMIT)
   return false;
  
  return true;
}

void setMisting(boolean power_on){
  if (power_on){
    digitalWrite(HMDPIN, HIGH);
    timer.enable(mist_timer);
    timer.enable(print_timer);
    Serial.print("Misting turned ON.\n");
    init_milli = millis();
    
  }
  else {
    digitalWrite(HMDPIN, LOW);
    Serial.print("Misting turned OFF.\n");
  }
  return;
}

// Print minutes since misting was turned on.
void printMistTime(){
  if (isMisting()){
    long interval = millis() - init_milli;
    float mist_time = interval / 60000.0;
    Serial.println("Misting for: "  + String(mist_time) + "/" + String(interval) + "(minutes/milliseconds) ");
  }
}

// Verifies if the box is being humidified.
boolean isMisting(){
  if (digitalRead(HMDPIN)){
    lcd.setCursor(13, 0);
    lcd.print("On ");
    return true;
  }
  lcd.setCursor(13, 0);
  lcd.print("Off");
  return false;
}

// Turns off misting, or not. Called by SimpleTimer.
void turnOffMisting(){
  if (h >= HUMID_UPPER_LIMIT){   // If lower than upper limit, skip turn off.   
    setMisting(false);
    timer.disable(mist_timer);
    timer.disable(print_timer);
  }
  
  return;
}

void setup() {

  // Setup serial channel.
  Serial.begin(9600); 
  Serial.println("Welcome to GrowBoxino!");
  
  // Setup misting timer para 3 minutos.
  mist_timer = timer.setInterval(360000, turnOffMisting);
  print_timer = timer.setInterval(30000, printMistTime);
  // Set the humidifier pin to output mode.
  pinMode(HMDPIN, OUTPUT);
  
  // Start the humidity/temperature sensor.
  dht.begin();
  
  // Start the LCD Display with the given number of columns and rows. 
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("Welcome!");
}

void loop() {
//  QueueList <String> strg_list;
  timer.run();
//  timer.disable(mist_timer);

  /* Reading and reporting temperature and humidity from sensor (DHT sensor). */
  
  // Read DHT. 
  h = dht.readHumidity();
  t = dht.readTemperature();

  // check if returns are valid, if they are NaN (not a number) then something went wrong, else report values.
  if (isnan(t) || isnan(h)) {
    Serial.println("Failed to read from DHT");
    lcd.print("DHT: Failed.");
  } else {
    Serial.print("Humidity: "); 
    Serial.print(h);
    Serial.print(" %\t");
    Serial.print("Temperature: "); 
    Serial.print(t);
    Serial.print(" *C\n");

    // lcd.clear();    
    lcd.setCursor(0, 0);
    lcd.print("Humid:" + String(h) + "%");
    lcd.setCursor(0, 1);
    lcd.print("Temp:" + String(t) + "C" );
  }

/*
  // Switch cooling and heating.
  if (coolBox(t) && !isCooling()) {
    setCooling(true);
  }
  else if (!coolBox(t) && isCooling()) {
    setCooling(false);
  }

  if (heatBox(t) && !isHeating()) {
    setHeating(true);
  }
  else if (!heatBox(t) && isHeating()) {
    setHeating(false);
  }
*/ 

  // Switch misting.
  if (mistBox() && !isMisting()){
    setMisting(true);
  } 

  delay(2000); 
  return;
}  

